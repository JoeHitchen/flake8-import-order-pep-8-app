# Flake8 Import Order PEP 8 App

A simple package that allows application packages to be separated from third-party packages when using [Flake8 Import Order](https://github.com/PyCQA/flake8-import-order)'s PEP 8 style.

