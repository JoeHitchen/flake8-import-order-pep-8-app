from flake8_import_order.styles import PEP8


class PEP8App(PEP8):
    accepts_application_package_names = True
